const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})
const app = express();
const PORT = 3007;
const taskRoutes = require(`./routes/taskRoutes`);

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))



app.use(`/api/tasks`, taskRoutes)



app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))