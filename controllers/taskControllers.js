 const Task = require(`./../models/Task`)
module.exports.createTask = async (reqBody) => {
	// let newTask = new Task({
	// 	name: reqBody.name
	// })

// 	return await newTask.save().then((savedTask, err) => {
// 		if(savedTask){
// 			return savedTask
// 		} else {
// 			return err
// 		}
// 	})
// }

   

	return await Task.findOne({name: reqBody.name}).then( (result, err) => {
		console.log(result)	

		if(result != null && result.name == reqBody.name){
			return true

		} else {
			
            let newTask = new Task({
				name: reqBody.name
			})

			return newTask.save().then((savedTask) => {
				if(savedTask){
					return savedTask
				} else {
					return err
				}
			})
		}
	})
}


module.exports.getAllTasks = async () => {

	return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

module.exports.deleteTask = async (id) => {
	return await Task.findByIdAndDelete(id).then(result => result)
}

module.exports.updateTask = async (name, reqBody) => {
	// console.log(reqBody)
// 		return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then(result => result)

return await Task.findOneAndUpdate({name: name}, {$set: reqBody}, {new:true})
	.then(response => {
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})

}

// activity
module.exports.getOneTask = async (id) => {

	return await Task.findbyId(id).then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

module.exports.updateTask = async (status, reqBody) => {

return await Task.findOneAndUpdate({status: status}, {$set: reqBody}, {new:true})
	.then(response => {
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})

}