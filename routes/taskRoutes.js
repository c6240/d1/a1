const express = require("express");

const router = express.Router();

const {createTask,getAllTasks,deleteTask,updateTask,getOneTask} = require(`./../controllers/taskControllers`)

router.post('/', async (req, res) => {
    
    try{
        await createTask(req.body).then(result => res.send(result))
    } catch(err){
        res.status(400).json(err.message)
    }

})

router.get('/', async (req, res) => {

    try{
        await getAllTasks().then(result => res.send(result))

    } catch(err){
        res.status(500).json(err)
    }
	
})

router.delete('/:taskId/delete', async (req,res) =>{
    
    try {
        await deleteTask(req.params.id),then(response => res.send(response))

    } catch(err){
        res.status(500).json(err)
    }
})


router.put('/:id', async (req,res) => {
    const id = req.params.taskId;
    
    // return await updateTask(id, req.body).then(response => res.send(response))

    await updateTask(req.body.name, req.body).then(response => res.send(response))
})

// activity
router.put('/task/:id/complete', async (req,res) => {
    const id = req.params.taskId;
    

    await updateTask(req.body.status, req.body).then(response => res.send(response))
})


	


router.get('/task/:id', async (req, res) => {

    try{
        await getOneTask(id).then(result => res.send(result))

    } catch(err){
        res.status(500).json(err)
    }
	
})



module.exports = router;